(ns de.uni-stuttgart.iaas.ipsm.protocols.core)

(defprotocol PersistenceProtocol
  (add-entity! [this entity] "Accepts
  an entity specified by the entity type. Entity type is a keyword and
  entity is a map.")
  (get-entity [this entity-id] "Get the entity
  type specified with the entity id")
  (get-all-entities [this entity] "Get all entities of the given type. :entity-type specifies type of
  entity and :entity-map specifies entity constraints")
  (update-entity-by-id! [this entity] "Get all entities of the given
  type")
  (delete-entity-by-id! [this entity] "Get all entities of the given type")
  (import-organizational-definitions! [this entity] "Used to import an organizational definitions to the persistence")
  (export-organizational-definitions [this] "Used to export an organizational definitions to the persistence"))

(defprotocol CoActProtocol)

(defprotocol ToscaPersistenceProtocol
  "TOSCA management layer, provides an interface to update manipulate TOSCA entities"
  (get-uri-for-added-service-template [_ m] "Adds a service template with all definitions and returns a URI targeting the service template. :defs is necessary containing at least one service template, :service-template-namespace and :service-template-id are not provided, the first oen is returned.")
  (get-service-template-on-location [_ m] "Returns the service template definitions of the provided service template URI, :service-template-uri field is necessary")
  (add-domain-manager-data! [_ m] "Adds TOSCA types into the respective TOSCA storage. Expects {(:defs | :types),:def-imports, :xsd-imports} maps.")
  (get-tosca-entities [_ m] "Returns the list of entities defined in :entity-data {:entity-type :node-type} :entity-types keyword has a precedence and used to return multiple entity types at once must be  a vector")
  (get-tosca-entity [_ m] "Returns a tosca entity of :entity-data {:entity-type :node-type :target-namespace \"namespace\" :id | :name \"id or name\" }")
  (delete-tosca-entities! [_ m] "Deletes the list of entities defined in :entity-data {:entity-type :node-type}")
  (delete-tosca-entity! [_ m] "Deletes a tosca entity of :entity-data {:entity-type :node-type :target-namespace \"namespace\" :id | :name \"id or name\" }")
  (add-tosca-entity! [_ m] "Adds a tosca entity :entity-data {:entity-type :node-type :target-namespace \"namespace\" :id | :name \"id or name\" }"))

(defprotocol ResourceOrganizer
  "An aggregator component for domain managers and execution environment integrators. Resources need to be manipulated using ToscaPersistence service."
  (get-domain-manager-data [_] "Returns all domain manager data maps. "))

(defprotocol RedoRuntime
  "A protocol that defined the IPE runtime service"
  (set-service-initials [_ service-initials] "To give a service initialization data, should be executed before starting the service!")
  (init-resource-driven-process-model! [_ process-model] [_ process-model instance-id] "Initializes process model with the given process model id. A process with process model id should be in persistent storage")
  (init-resource-driven-process-using-entity-identity! [_ entity-identity] "Initializes process model referred by the given entity identity. A process with process model id MUST be in persistent storage")
  (terminate-process-execution! [_ process-id] "Process with the recieved id will be terminated, id is a string")
  (update-process-instance! [_ new-process-instance-model] "New process instance model contains")
  (get-process-instance [_ process-id] "Return process instance with the given id. If not found returns null")
  (list-processes [_] "Returns the list of all available process ids")
  (list-running-processes [_] "Returns the list of running processes")
  (list-terminated-processes [_] "Returns the list of terminated processes"))

(defprotocol ExtendableConceptProtocol (get-attr [_ attr] "Returns the keyword defined by the attr object."))

(defprotocol FormatTransformer (convert-maps-into-exchange [m & ms])
             (convert-exchange-into-maps [e]))

(defprotocol TransformableConceptProtocol (get-entity-type [_]
                                                           "Returns the type of the contained entity, e.g., resource, action,
  activity, interaction, etc.")  (get-property-list [_] "Returns the
  type of the contained entity, e.g., resource, action, activity,
  interaction, etc.")  (get-entity-representation [_ format] "Returns
  entity in the given representation format") (load-data [this input
                                                          input-format] "Load the given data in the given into entity map,
  transformer needs to support this"))

(defprotocol TransformerProtocol "A protocol for transforming between
  different resource-driven process formats" (get-name [_] "Name of the
  transformer") (get-description [_] "Description of the transformer")
             (transform-entity [_ transformation-map] "A map similar to
  {:target-entity :owl :entity-data {:entity-type :resource-driven-process
  :entity-map {}}}") (load-all-possible-entities [_ source] "Input a
  data and load it to the implementing type") (load-from [_ source]
                                                         "Input a data and load it to the implementing type")
             (get-transformable-formats [_] "Return the formats that this
  transformer transforms into") (get-loadable-formats [_]))

(defprotocol PreprocessorProtocol "A protocol for converting a raw
  data format such as String to processable format such as
  OWLOntology" (applicable-types [_] "Return set-of types to be
  applied") (pre-process [_ raw-format] "Operation of
  pre-processing"))

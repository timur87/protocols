package uni_stuttgart.ipsm.protocols.integration.operations;

import java.io.InputStream;
import java.net.URI;
import java.util.List;
import javax.xml.namespace.QName;
import org.oasis_open.docs.tosca.ns._2011._12.TOperation;

/**
 * An operation realization for instance a realization of the operation
 * initialize for a resource
 *
 * @author timur
 *
 */
public interface OperationRealization {

    /**
     * Returns the name of the interface this operation belongs to
     *
     * @return
     */
    String getInterfaceName();

    /**
     * Returns type definitions for this object. That's a schema file.
     *
     * @return
     */
    InputStream getTypeDefinition();

    /**
     * Returns the list of qnames of the deployables that are used by
     * this operation. Only these types are given as RunnableContainer
     * to this operation during execute. These must be provided by
     * registered corresponding domain managers so that they can be
     * delivered.
     *
     * @return
     */
    List<QName> getRequiredDeployableTypes(QName resourceType);

    /**
     * Returns targetNamespace for the parameter definitions. It should be a
     * URI. Target namespace of these parameters
     *
     * @return
     */
    URI getTypeDefinitionsTargetNamespace();

    /**
     * Operation definition
     *
     * @return
     */
    TOperation getOperationDefinition();

    /**
     *
     * @param resourceOrRelationship Target resource or relationship model
     * @param callback Operation callback
     * @param parameters Parameters expected
     * @return
     */
    void executeOperation(List<RunnableContainer> resourceOrRelationshipContainer, Object inputParameters, OperationCallback callback);

}

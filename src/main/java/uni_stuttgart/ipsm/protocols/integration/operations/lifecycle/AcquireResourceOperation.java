package uni_stuttgart.ipsm.protocols.integration.operations.lifecycle;




import java.net.URI;

import javax.xml.namespace.QName;

import org.oasis_open.docs.tosca.ns._2011._12.TOperation;
import org.oasis_open.docs.tosca.ns._2011._12.TParameter;

import de.uni_stuttgart.iaas.ipsm.v0.TOperationMessage;
import uni_stuttgart.ipsm.protocols.integration.operations.OperationCallback;


public abstract class AcquireResourceOperation extends BaseLifecycleResourceOperation {

	public static String OPERATION_NAME = LIFECYCLE_INTERFACE_TARGET_NAMESPACE + "acquire-resource";
	// name of the type must match the XSD property specification
	public static String DEPENDENCIES_PARAMETER_NAME = "Dependencies";
	// name of the type must match the XSD property specification
	public static String PRECONDITINIONER_PARAMETER_NAME = "PreconditionerRelationships";	

	// name of the type must match the XSD property specification
	public static String INSTANCE_LOCATION_PARAMETER_NAME = "InstanceLocation";	

	
	@Override
	public TOperation getOperationDefinition() {
		// define acquirement operation
		TOperation operation = new TOperation();
		operation.setName(OPERATION_NAME);
		//Input perameters
		// Preconditioner relationships
		// All relationships that are needed during the acquirement
		TParameter preconditionerRelationshipsParam = new TParameter();
		preconditionerRelationshipsParam.setName(PRECONDITINIONER_PARAMETER_NAME);
		preconditionerRelationshipsParam.setType((new QName(getTypeDefinitionsTargetNamespace().toString(), PRECONDITINIONER_PARAMETER_NAME)).toString());

		// Dependency resource models
		TParameter dependenciesParam = new TParameter();
		dependenciesParam.setName(DEPENDENCIES_PARAMETER_NAME);
		dependenciesParam.setType((new QName(getTypeDefinitionsTargetNamespace().toString(), DEPENDENCIES_PARAMETER_NAME)).toString());
		
		// Dependency resource models
		TParameter instanceStateParam = new TParameter();
		instanceStateParam.setName(INSTANCE_LOCATION_PARAMETER_NAME);
		instanceStateParam.setType((new QName(getTypeDefinitionsTargetNamespace().toString(), INSTANCE_LOCATION_PARAMETER_NAME)).toString());

		TOperation.InputParameters inputParameters = new TOperation.InputParameters();
		inputParameters.getInputParameter().add(preconditionerRelationshipsParam);
		inputParameters.getInputParameter().add(dependenciesParam);
		operation.setInputParameters(inputParameters);
		
		TOperation.OutputParameters outputParameters = new TOperation.OutputParameters();
		outputParameters.getOutputParameter().add(instanceStateParam);
		operation.setOutputParameters(outputParameters);
		
		return operation;
	}
	
	public void completeProcessExecutionSuccessfully(URI resourceLocation, OperationCallback callback){
		TOperationMessage outputParameters = new TOperationMessage();
		outputParameters.setInstanceLocation(resourceLocation.toString());
		outputParameters.setInstanceState(":engaged");
		callback.onSuccess(outputParameters);
	}

	public void completeProcessExecutionwithError(String errorDescription, OperationCallback callback){
		TOperationMessage outputParameters = new TOperationMessage();
		outputParameters.setErrorDefinition(errorDescription);
		outputParameters.setInstanceState(":erroronous");
		callback.onError(outputParameters);
	}

}

package uni_stuttgart.ipsm.protocols.integration.operations;

import java.net.URI;
import java.util.List;

import javax.xml.namespace.QName;

public interface ResourceOperationRealization extends OperationRealization {
    
    /**
     * Returns the list of resource types supported by this operation. In case a
     * domain specific operation, this returns null and all are supported.
     * 
     * @return List of resource {@link QName} specified in a domain
     *         
     */
    List<QName> getSupportedResources();
    
    /**
     * Get valid domains for the operation
     * 
     * @return List of all namespaces that are supported by this operation as
     *         {@link URI}s
     */
    List<URI> getSupportedDomains();
    
}

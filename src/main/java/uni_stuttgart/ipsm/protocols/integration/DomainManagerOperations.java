package uni_stuttgart.ipsm.protocols.integration;

import java.net.URI;
import java.util.List;

import javax.xml.namespace.QName;

import org.oasis_open.docs.tosca.ns._2011._12.Definitions;

import de.uni_stuttgart.iaas.ipsm.v0.TIntentionDefinition;

/**
 * This class needs to be implemented as a provider implementation.
 *
 * @author sungurtn
 *
 */
public abstract interface DomainManagerOperations {
    /**
     * TOSCA Definitions file that contains different resources and relationships
     * the list of individuals of resources.
     * @return
     */
    Definitions listDomain();


    /**
     * Resource type and information about the intention. Result returns a DeployableArtifact
     * @param
     * intentionInformation defines the intention that this resource relates to
     * type of the desired resource or relationship
     * @return
     */
    Deployable getDeployable(QName resourceQname, QName deployableType, TIntentionDefinition intention);


    /**
     * List types of available deployables for a specific resource type
     * @param
     * resourceType is the type of resource provided by this domain manager
     * @return List of the QNames representing different type of
     * deployables provided for this resource type actual deployable
     * is provided by getDeployable
     */
    List<QName> listDeployablesOfResource(QName resourceType);


    /**
     * Namespaces of the provided resources by this domain manager
     * It's a URI
     * @return
     */
    URI getTargetNamespace();


    /**
     * To get the actual imported data
     * @return
     */

    List<Import> getImports();

}
